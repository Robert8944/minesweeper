package game;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

public class GameBoard extends JFrame implements ActionListener{
	private static final long serialVersionUID = -2639787148809491413L;
	
	public int bx = 10, by = 10, bm = 3, numHidden = 100;
	public Tile[][] tiles = new Tile[10][10];
	
	public GameBoard(){
		this.setVisible(true);
		this.setSize(500,500);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		
		this.fillArray(true);
		this.setUp();
		this.setSize(501,500);
		this.setSize(500,500);
		
	}

	public static void main(String[] args) {
		new GameBoard();
	}
	
	public void setUp(){
		this.setLayout(new GridLayout(bx,by));
		for (int i=0; i<bx; i++){
			for (int j=0; j<by; j++){
				this.add(tiles[i][j]);
				//System.out.println("HI");
			}
		}
	}
	
	public void fillArray(boolean ns){
		if(ns){
			boolean changed = false;
			while (changed==false){
				changed = true;
				try{
					bx = Integer.parseInt(JOptionPane.showInputDialog(this, "Rows?", "Rows?", JOptionPane.QUESTION_MESSAGE));
				}catch(Exception ex){changed = false;}
			}
			changed = false;
			while (changed==false){
				changed = true;
				try{
					by = Integer.parseInt(JOptionPane.showInputDialog(this, "Columns?", "Columns?", JOptionPane.QUESTION_MESSAGE));
				}catch(Exception ex){changed = false;}
			}
		}
		numHidden = bx*by;
		boolean changed = false;
		while (changed==false){
			changed = true;
			try{
				bm = Integer.parseInt(JOptionPane.showInputDialog(this, "Mines?", "Mines?", JOptionPane.QUESTION_MESSAGE));
			}catch(Exception ex){changed = false;}
			if(bm>=by*bx){
				changed = false;
			}
		}
		int numSet = 0;
		tiles = new Tile[bx][by];
		for (int i=0; i<bx; i++){
			for (int j=0; j<by; j++){
				tiles[i][j] = new Tile(i,j);
				tiles[i][j].addActionListener(this);
			}
		}
		while(numSet<bm){
			tiles[(int) (Math.random()*bx)][(int) (Math.random()*by)].setMine(true);
			numSet++;
		}
	}
	
	public Tile[] getProx(Tile t){
		Tile[] prox = new Tile[8];
		if(t.x+1 < bx && t.y-1 >=0)
			prox[0] = tiles[t.x+1][t.y-1];
		if(t.x+1 < bx)
			prox[1] = tiles[t.x+1][t.y];
		if(t.x+1 < bx && t.y+1 < by)
			prox[2] = tiles[t.x+1][t.y+1];
		if(t.y+1 < by)
			prox[3] = tiles[t.x][t.y+1];
		if(t.y-1 >= 0)
			prox[4] = tiles[t.x][t.y-1];
		if(t.x-1 >= 0 && t.y+1 < by)
			prox[5] = tiles[t.x-1][t.y+1];
		if(t.x-1 >= 0)
			prox[6] = tiles[t.x-1][t.y];
		if(t.x-1 >= 0 && t.y-1 >=0)
			prox[7] = tiles[t.x-1][t.y-1];
		return prox;
	}
	
	public void actionPerformed(ActionEvent ae) {
		Tile tt = (Tile)ae.getSource();
		tt.wasClicked = true;
		numHidden--;
		if(tt.isMine){
			JOptionPane.showMessageDialog(this, "YOU LOSE!!!");
			this.fillArray(true);
			this.setUp();
			this.setSize(501,500);
			this.setSize(500,500);
		}else{
			Tile[] proxTiles = this.getProx(tt);
			int numProx = 0;
			for (int i=0;i<proxTiles.length;i++){
				if(proxTiles[i] != null && proxTiles[i].isMine){
					numProx++;
				}
			}
			tt.setText(numProx + "");
			if (numProx == 0){
				for (int i=0;i<proxTiles.length;i++){
					if(proxTiles[i] != null && proxTiles[i].wasClicked==false){
						proxTiles[i].wasClicked = true;
						proxTiles[i].doClick();
					}
				}
			}
		}
		if(numHidden == bm){
			JOptionPane.showMessageDialog(this, "YOU WIN!!!!");
			this.fillArray(true);
			this.setUp();
			this.setSize(501,500);
			this.setSize(500,500);
		}
	}
	
}
