package game;

import javax.swing.JButton;

public class Tile extends JButton{
	private static final long serialVersionUID = 5094096497924123944L;
	
	public int x = 0, y = 0;
	public boolean isMine = false;
	public boolean wasClicked = false;
	
	public Tile(int tx, int ty){
		super();
		x = tx;
		y = ty;
	}
	
	public void setMine(boolean mine){
		isMine = mine;
	}
	

}
